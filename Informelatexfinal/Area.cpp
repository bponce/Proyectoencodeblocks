/*Autor: BRYAN MIGUEL PONCE PAREDES
***********************************
UNIVERSIDAD NACIONAL DE SAN AGUSTIN
ESCUELA PROFESIONAL DE INGENIERIA TELECOMUNICACIONES
****************************************************
CURSO : COMPUTACION I
FECHA : 12 OCTUBRE 2017
ENUNCIADO:
Hacer un programa que calcule el area longitud y volumen de una circunferencia apartir de su radio
*/





#include<iomanip>
#include<math.h>
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<iostream>
#define pi 3.1415169
using namespace std;

int main()
{
    float r, D, R, perimetro, area, volumen;
    cout<<"Introduce el radio de la circunferencia para hallar su longitud"<<endl;
    cin>>r;
    perimetro=2*pi*r;
    cout<<"El perimetro de la circunferencia es:"<<endl<<perimetro<<endl;
    cout<<"Introduce el radio de la circunferencia para hallar el area"<<endl;
    cin>>D;
    area=(pi*D*D)/4;
    cout<<"El area de la circunferencia es:"<<endl<<area<<endl;
    cout<<"Introduce el radio de la circunferencia para hallar su volumen a partir de este"<<endl;
    cin>>R;
    volumen=(4*pi*r*r*r)/3;
    cout<<"El volumen de la circunferencia a partir de su radio es:"<<endl<<volumen<<endl;
}
