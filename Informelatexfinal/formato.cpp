/*Autor: Bryan Miguel Ponce Paredes
*************************************
UNIVERSIDAD NACIONAL DE SAN AGUSTIN
ESCUELA PROFESIONAL DE INGENIERIA TELECOMUNICACIONES
****************************************************
CURSO : COMPUTACION 1
FECHA 25 SEPTIEMBRE 2017
Enunciado:
Ejemplo de salida estandar con setfill y setw
*/
#include <iostream>
#include<iomanip>

using namespace std;

int main()
{
    //Variables
    double a1 = 205.549;
    double a2 = 4.18;

    //Proceso
    //Salida normal
    cout<< setw(60) <<"ESTE PROGRAMA VISUALIZA SALIDA ESTANDAR MEJORADA\n\n";
    cout << "a1->" << a1 <<endl;
    cout << "a2->" << a2 <<endl;
    cout << endl;

    //Salida utilizando fixed relleno de ceros.
    cout << fixed << "a1->" << a1 << endl;
    cout << "a2->" << a2 << endl;
    cout << endl;

    //Salida precision de 1 decimal.
    cout << "a1->" << setprecision(1) << a1 << endl;
    cout << "a2->" << a2 <<endl;
    cout << endl;

    //Salida con relleno de simbolo
    cout << "a1->" << setw(10) << setfill('.') << a1 << endl;
    cout << "a2->" << a2 << a2 << endl;
    cout << "a1->" << setw(12) << a1 << endl;
    cout << endl;

    system("pause");
    return 0;

}
