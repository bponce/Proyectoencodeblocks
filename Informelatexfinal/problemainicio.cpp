/*Autor: BRYAN MIGUEL PONCE PAREDES
***********************************
UNIVERSIDAD NACIONAL DE SAN AGUSTIN
ESCUELA PROFESIONAL DE INGENIERIA TELECOMUNICACIONES
****************************************************
CURSO : COMPUTACION I
FECHA : 12 OCTUBRE 2017
ENUNCIADO:
Hacer un programa que convierta un numero de segundos en horas, minutos y segundos
*/



#include <iostream>

using namespace std;

int main()
{

    //Constantes
    const int HORA   = 3600;
    const int MINUTO = 60;

    //Variables
    int t,h,m,s;

    //Entrada
    cout <<  "Tiempo en segundos:"; cin>> t;

    //Proceso
    h = t / HORA;
    t = t % HORA;
    m = t / MINUTO;
    s = t % MINUTO;

    //SALIDA
    cout <<"\n";
    cout <<"Hora: "<<h<<"\n";
    cout <<"Minuto: "<<m<<"\n";
    cout << "Segundo : "<<s<<"\n";
    return (0);
}


